import time

import netCDF4 as nc
import numpy as np

from src.interp import InterpolateKernel
from scipy.interpolate import RectBivariateSpline
from ncBuilder import (
    ncBuilder,
    ncHelper,
    )

# nc_file = nc.Dataset('something.nc', 'r', keepweakref=True)

import pandas as pd
nc_file = nc.Dataset('empty.nc', 'w', diskless=True)
_fake_lat, _fake_lon = np.arange(-50, 10, 0.25), np.arange(-80, -20, 0.25)
_fake_time = pd.date_range('2021-01-01', periods=30, freq='H').to_pydatetime()
ncBuilder.create_nc(nc_file, _fake_lat, _fake_lon, time=_fake_time, vars={'var1': {'dims': ('time', 'latitude', 'longitude')}})
ncBuilder.update_nc(nc_file, 'var1', np.random.rand(_fake_time.size, _fake_lat.size, _fake_lon.size))

times = ncHelper.load_time(nc_file['time'])
lat, lon = ncHelper.get_lats_lons(nc_file)
grids = nc_file.variables['var1'][:]

region = [lat[0], lon[0], lat[-1], lon[-1]]
in_res = np.diff(lat).mean()
out_res = 0.043
radius = 400.

target_lat = np.arange(region[0], region[2] + out_res / 2, out_res)
target_lon = np.arange(region[1], region[3] + out_res / 2, out_res)

print('creating out nc')
nc_out = nc.Dataset('./epe_regrid_test.nc', 'w')
ncBuilder.create_nc(nc_out, target_lat, target_lon, time=times, vars={'idw': {'dims': ('time', 'latitude', 'longitude')},
                                                                      'adw': {'dims': ('time', 'latitude', 'longitude')},
                                                                      'spl': {'dims': ('time', 'latitude', 'longitude')},
                                                                      })
                                                                     
def time_it(_func):
    def _f(*args, **kwargs):
        start = time.time()
        _res = _func(*args, **kwargs)
        end = time.time()
        print('Total time elapsed: %fs'%(end - start))
        return _res
    return _f

start = time.time()    
print('creating kernel map')
interp = InterpolateKernel(region, in_res, out_res, radius=radius)
end = time.time()
print('Time to build kernels: %fs'%(end - start))
    
@time_it                                                                     
def run(region, in_res, out_res, radius, grids, target_lat, target_lon, interp):
    print('starting to interpolate')
    # idw_res = np.array([interp.idw(grid) for grid in grids])
    # adw_res = np.array([interp.adw(grid) for grid in grids])
    idw_res = interp.idw_n_channels(grids.transpose((1, 2, 0))).transpose((2, 0, 1))
    adw_res = interp.adw_n_channels(grids.transpose((1, 2, 0))).transpose((2, 0, 1))
    spl_res = np.array([RectBivariateSpline(lat, lon, grid)(target_lat, target_lon) for grid in grids])
    print('done interpolating')
    
    return idw_res, adw_res, spl_res

idw_res, adw_res, spl_res = run(region, in_res, out_res, radius, grids, target_lat, target_lon, interp)
                                                                     
ncBuilder.update_nc(nc_out, 'idw', idw_res)
ncBuilder.update_nc(nc_out, 'adw', adw_res)
ncBuilder.update_nc(nc_out, 'spl', spl_res)

nc_out.close()
